# A propos de moi



### Je suis Lisa-Marie Merchat,

Je suis étudiante en architecture et diplômé en Deisgn d'Espace basée sur Montpellier. Je suis actuellement en voyage d'études pour 1 an en Belgique.

Vous pouvez voir sur ce site une petite partie de mes travaux en école d'architecture.

![](images/photo_de_profil_gitlab.jpg)

©Marion Jordan Photography


## Formations antérieures

Je suis née à Montpellier dans le Sud de la France. Après avoir passé toute mon enfance en périphérie. J'ai choisi d'étudier en ville à Avignon pendant 2 ans pour un baccalauréat en design et Art Appliqués.

Cette filière m'a amené à pratiquer le dessin pendant 2 ans et m'a pris de passion pour le dessin d'architecture, le design et la mode. Un Bts Design d'Espace à l'Ecole Superieur des Métiers Artistiques à Montpellier m'a permis de combiner l'architecture et le design produit.
    
Son obtention a été un tremplin vers l'intégration de l'Ecole d'architecture en 2ème année à l'Ecole Nationale d'Architecture de Montpellier. Je suis actuellement à l'école d'architecture la cambre Horta de Bruxelles.


## Précédents travaux

Les travaux qui suivent rendent compte de mes derniers projets en architecture.

### LA CITE BLANCHE

La cité blanche est une résidence qui s’inscrit dans une démarche contemporaine en vue de transformer le littoral du Grau du Roi. Le projet est un exercice de deuxième année de licence d'architecture individuel accompagné par le professeur Laurent DUPORT à l'ENSAM en 2019.

![This is another caption](images/la_cite_blanche_gitlab.jpg)

©LMM

### L'IMMEUBLE A VELO

Le projet est une démarche d’intégration du vélo dans un projet de logements collectif. Cela apporte une nouvelle dimension au projet dans la prise de hauteur sur deux roues. On désinhibe l’action de « rentrer chez soi » en intégrant un nouveau mode d’accès, d’habiter et de vivre ensemble dans une démarche de bien-être corporelle et mentale. Le projet est un exercice de fin de licence d'architecture individuel accompagné par le professeur Emmanuel NEBOUT à l'ENSAM en 2020.


![This is another caption](images/sample-photo.jpg)

©LMM

## L'objet de recherche - TELEGONO / Lampe de table


![](images/Photo_Lampe.png)

©LMM

Auteur : Vico MAGISTRETTI

Date : 1969

Matériau : ABS,PP, electronic circuit, Artemide, ITA

Lieu : Belgique

Exposé à : Brussels Design Museum


### IMPRESSION

J'ai choisi cette lampe car j'aime le concept modulable de réglage de la lumière. L’œuvre offre au spectateur un nouvel aspect de luminosité. L’implication manuelle est ludique et permet de s'approprier l'objet. L’utilisation de la couleur franche m'a attiré et je pense que la boule blanche au centre doit créer un contraste intéressant une fois allumée. Peut-être comme une pleine lune dans un ciel de nuit noir.


### Merci d'avoir regardé mon profil !
