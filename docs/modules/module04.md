# 4. Découpe Laser / Conception d'une lampe

Cette semaine nous apprenons à utiliser la machine de découpe laser.
Pour commencer Victor Levy nous a présenté le travail à réaliser. 
La demande est de réaliser un abat-jour  avec une feuille en polypropylène de 60x80. Pour concevoir l'objet il faut penser en fonction de la machine, ainsi on pourra plus facilement comprendre l'objet.

L'objectif est de réaliser un patron de notre abat jour en papier, de le modéliser sur fusion 360 puis d'imprimer son abat-jour.

# DECOUPE ASSISTEE PAR ORDIANTEUR :

### Qu'est ce que c'est une découpe laser ?

J'ai d'abord effectué des recherches plus approfondies pour découvrir cette technique que j'ai pu utiliser dans mon école de base : l'ENSAM de Montpellier. Dans mon école des personnes s'occupent de réaliser le travail pour nous et nous sommes seulement témoins du résultat final.

Il est alors important de connaitre son principe et ses dangers avant de l'utiliser.
Plusieurs sites m'ont permis de mieux comprendre le principe mécanique.

Pour résumer ce qu'est une machine de découpe laser, je peux dire que c'est un rayon laser qui par un rayonnement permet d'enlever de la matière selon son intensité. Les paramétrages permettent alors que faire des variantes différentes en fonction du matériau et du résultat souhaité. Ainsi, cette technique permet une découpe ou gravure précise, nette et rapide.

Ci-dessous vous trouverez les liens si vous souhaitez connaitre les caractéristiques approfondies.

https://fr.wikipedia.org/wiki/D%C3%A9coupe_laser

https://www.laboiteadecoupe.fr/decoupe-laser-industrielle/ 

http://ww.makilab.org/images/c/cd/Formation_Laser_advanced_-_Makilab_-_11_oct_2015.pdf 

L'ensemble des informations concernant les machines du FabLab sont aussi disponibles sur la page du FabZEro -> Machines.


### Qu'elles sont les machines du FabLab ?

![](../images/machines_laser.png)

Le FabLab dispose de deux machines pour procéder à la découpe Laser.

- La Lasersaur : Cette machine est la plus grande du FabLab en terme de découpe laser. Elle permet de travailler sur des grands fichiers avec un espace de travail de 122x61cm. Il est nécessaire que les fichiers soient en SVG ou DFX (les SVG fonctionnent tout de même mieux). On utilise Drive BoardApp pour se servir de la machine directement au FabLab. On peut passer sur le logiciel INKSCAPE installé sur l'ordinateur du FabLab pour créer ces dessins vectoriels. C'est un logiciel gratuit disponible pour Linux, Windows et macOs.

INKSCAPE disponible en téléchargement ici : https://inkscape.org/fr/

- La Muse Full Spectrum : Cette machine est plus petite et moins puissante et sert plutôt pour de la gravure sur des petites pièces. Ca surface de travail est de 50x30cm. A contrario de la Lasersaur, la machine peut lire en plus du SVG des fichiers différents comme les images matricielles BMP, JPG, PNG, TIFF ou PDF.


### Qu'elles ont les différentes options que l'on peut effectuer avec la machine ?

Les machines de découpe laser ne sont pas multi-tâches mais permettent la découpe et la gravure. La différence des processus dépend finalement des réglages. On peut choisir la profondeur du marquage sur plusieurs supports. Pour cela le FabLab dispose d'un ensemble de matières où des tests ont déjà été effectué. Cela nous permet de faire le choix de son intervention.

On peut nous aussi faire des tests sur un matériau avec un fichier qui se situe sur l'ordinateur du FabLab et ainsi agrandir la collection de palette de matériaux existants.

Pour utiliser la découpe ou la gravure, on attribue une couleur au trait vectoriel. Chaque couleur est paramétrée en fonction de sa puissance % et sa vitesse F.

Exemple de mesure issu du site : https://wiki.chantierlibre.org/machines:lasersaur:reglages_lasersaur

![](../images/exemple_reglage_lasersaur.png)


### Qu'elles sont les règles ?

**L'UTILISATION DES MACHINES**

Il y a plusieurs règles de base pour que tout se passe toujours bien lors de la découpe :

- Bien connaitre son matériau (pour ne pas créer dégrader la machine ou soi-même)
- Toujours ouvrir la vanne d'air comprimé (avant que la machine soit en route)
- Toujours allumer la pompe à air comprimé (avant que la machine soit en route)
- S'il y a un problème appuyé sur le bouton rouge d'urgence (Sur l'écran pour la Muse FullSpectrum)


**LES MATERIAUX**

On ne peut pas utiliser tous les matériaux avec la machine de découpe laser.

Ceux-ci sont recommandés :

- Bois contreplaqué de 3/4mm
- Acrylique (Plexiglass)
- Papier
- Carton

Ceux-ci sont déconseillés :

- MDF
- ABS,PS
- Polyétylène épais, PE, PET, PP
- Matière fibreuse : fibre de verre, fibre de carbone
- Métaux 

Ceux-ci sont **INTERDIT** (Dangereux pour vous et la machine !) :

- PVC
- Cuivre
- Téflon
- Vinyle
- Résine (phénolique ou epoxy) 


**LA CONNECTION AUX MACHINES**

On ne peut utiliser qu'un seul ordinateur par machine. Ainsi on doit se connecter au réseau ou utiliser un cable RG45.

Le réseau : LaserCutter / Le MDP : fablabULB2019



**UTILISER LES MACHINES**

Il y a différentes étapes pour utiliser les machines :

- 1 . DEMARER LA MACHINE : Tourner le bouton d'arrêt d'urgence pour l'allumer (Pour la Lasersaur) / Se connecter au réseau, wifi ou câble / Retourner à l'origine en cliquant sur "Run Homing Cycle" / Vérifier le statut de la machine (logo vert = ok ; logo orange = vanne non ouverte ; logo rouge = machine endommagée)

- 2 . POSITIONNEMENT : Installer le matériau / Déplacer la tête de découpe à l'origine à l'aide du bouton move ou jog (relever la lentille avant) / Régler la focale à l'aide des cales à dispositions (15mm de distance)

- 3 . REGLAGES : Le bouton "Offset" permet de déplacer le dessin sur la feuille en cas d'erreur / Le bouton "Run Bounding Box" permet de voir que le tracer ne dépasse pas du matériau / Calibrer la découpe à l'aide des couleurs de traits (bouton +, ajout d'autant de couleur que de découpes ou gravures) / Réglage de la vitesse % et puissance F

- 4 . LANCER L'IMPRESSION (Commencer par la gravure avant la découpe)



# MON UTILISATION DE LA MACHINE ET REALISATION :

## Premier essai

### Phase 1 - Réalisation du prototype en papier

Nous disposons d'une feuille A3. Je me suis demandé comment je pouvais reprendre la forme de la base de ma lampe en pliage. J'ai alors fait une bête recherche sur internet de sphère en papier. Un schéma de la composition d'une sphère m'a permise de trouver l'idée. Une sphère en une succession de demi-cercle en révolution à partir d'un axe central.

C'est la méthode que j'avais établie sur Fusion 360 en utilisant le profil de ma lampe et lui donnant un axe de rotation de 180°.
Je choisis de reprendre le profil de ma lampe pour créer une version différente de la maquette produite le 15 octobre.

![](../images/sphere.png)

©Sphère Wikipédia

Lors de la confection des maquettes papiers je me suis rendu compte qu'une des propositions fonctionnait beaucoup mieux. La proposition n°1 est basée sur un système en acordéion. Le système en acordéion ne donnait pas la forme que je voulais jusqu'a ce que je colle deux faces ensemble.


![](../images/idee_guirelande_dessin.png)

©LMM

Le système que j'ai dessiné n'est pas le même que celui réalisé en plan puis en 3D manuel. Je pense que revoir le modèle pour qu'il soit plus stable à la base sera plus judicieux.


![](../images/maquette_guirelande_1.png)

©LMM

![](../images/maquette_guirelande_2.png)

©LMM

![](../images/maquette_guirelande_3.png)

©LMM

Grâce au patron je peux voir que j'aurais besoin d'un enchainement d'un moins 12 profil pour que la réalisation est 6 bras.
Je prends une base de 2,5mm par arc de cercle. Vu que les formes finales seront assez petites pour un abat-jour j'ai eu l'idée d'une guirlande et de faire plusieurs bandes sur ma feuille en polypropylène. Les bras auront une épaisseur de 5mm.

Je dois laisser une forme libre en bout pour pouvoir coller deux profils ensemble et fermer les formes.

J'établit un nouveau profil maintenant que je sais que l'idée réaliser en maquette fonctionne. Je peux modéliser l'objet sur Fusion 360 pour avoir le prototype en 3D.


### Phase 2 - Réalisation du prototype en 3D sur Fusion 360

Je choisis d'abord de réaliser le patron de mes tetes de lampe sur fusion 360 donc je choisis de faire un tracé sur les axes x,y.

![](../images/guirelande_profil_fusion360.png)

©LMM / Fusion 360

Je me suis rendu compte que je ne pouvais pas exporter mon fichier plan fusion 360 et que je devrais passer sur un autre logiciel pour le tracé, comme Archicad.

Je me demande comment faire tenir l'ensemble par emboitement et non en accordéon et collage, ainsi, on aurait une pièce facile de reproduction.


## Deuxième essai

### Phase 1 - Réalisation du prototype en papier

Le prototype en papier découle de l'idée globale et de la matière que j'ai. Je pense que j'ai voulu créer ma forme et la finalité avant même de penser au sens de ma lampe.

Je me base sur la réalisation d'une guirlande de 25 lampes. La première étape fut d'acheter une guirlande et d'en analyser les caractéristiques pour que les boules s'emboitent sans ajout de matière comme de la colle mais plûtot par pliage ou assemblage.

![](../images/guirelande.png)

©LMM

![](../images/guirelande_taille.png)

©LMM

La forme générale en profil doit faire 3 cm de la base à la lumière. J'organise donc mon profil autour de cette mesure.

![](../images/guirelande_essai_2_dessin.png)

©LMM


### Phase 2 - Réalisation du prototype en 3D sur Fusion 360

![](../images/guirelande_fusion.jpg)

©LMM / Fusion 360

Je peux voir que ma forme fonctionne et la matérialité transparente me permet de mieux visualiser l'objet final.
Je dois maintenant organiser ma feuille pour obtenir 25 boules, soit 200 profils et 25 éléments pour maintenir les ensembles.

### Phase 3 - Tracage de ma forme final en 2D

Nous avons eu une formation en début d'après-midi qui nous permet de découvrir les machines de découpe laser. Le traçage de l'objet à plat doit se faire sur un autre logiciel que fusion 360, on peut par exemple utiliser Autocad, Archicad ou Illustrator tant qu'on arrive à un fichier en SVG ou DXF en couleur RVB avec des traits non groupés pour utiliser la machine Lasersaur.

Je trace les lignes de découpe sur le logiciel Archicad en utilisant la couleur rouge. Malheureusement on ne peut pas exporter en SVG avec Archicad. J'ai donc exporté en DXF.

![](../images/guirelande_tracage_archicad.jpg)

### Phase 4 - La découpe et l'assemblage de la lampe

Avant la découpe une amie m'a aidé à passer on fichier en SVG avec Inkscape.

![](../images/etapes_decoupe.png)

Je n'ai eu aucun problème avec mon fichier sauf pour l'assemblage. Les encoches réalisées sont trop grandes. J'ai alors recréé une pièce pour tenir l'ensemble avec Autocad puis je l'ai mis en SVG sur Inkscape.

![](../images/etape_decoupe_2.png)

Je découpe seulement les 25 nouveaux éléments sur la chute.
Ce fut un échec car des pièces ne se sont pas découpé correctement.. J'ai relancé le fichier une deuxième fois. Je retiens qu'il faut imprimer plus de pièces que prévues pour que qu'il n'y est pas de manque.

Les pièces ne fonctionnent pas non plus, en supperposant deux pièces on a déjà un résultat plus solide.

![](../images/essai_montage_2.png)

### Création d'une lampe avec la chute

![](../images/lampe_negatif.png)

