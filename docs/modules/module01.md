# 1. Documentation

Voici l'ensemble des recherches qui m'ont permis de comprendre un peu plus l'utilisation de GitLab, Git et le Markdown.


## LE MARKDOWN

### QU'EST CE QUE LE MARKDOWN ?

C'est un language de balisage facile à lire. Beaucoup de sites utilisent le Markdown car c'est un language universel.
Il ne se substitue quand même pas à HTML car les fonctions qu'il propose sont réduites.

Le nom « Markdown » est en fait un jeu de mots. Le terme anglais désignant les langages de balisage est « Markup Languages ». Le nom Markdown signifie clairement qu’on a affaire à un langage simplifié et léger, d’où l’appellation : Markdown.


### LES MISES EN FORMES


1. ECRIRE EN **GRAS** ET *ITALIQUE* / Il suffit d'utiliser des étoiles (astérixes). Pour écrire en italique, insérez tout simplement une étoile devant et derrière l'expression le mot ou concerné. Pour le gras, insérez deux étoiles avant et après.

2. **LES TITRES** / On peut créer jusqu'à 6 niveaux de sous-titres en ajoutant le dièse. (On le sépare du texte avec un espace)

3. **TEXTE BARRÉ** / Pour barrer un texte avec Markdown, précédez-le de deux tildes et refermez la chaîne avec deux autres tildes.

~~Test texte barré~~

4. **PARAGRAPHES** / Le langage Markdown utilise des sauts de lignes pour créer des paragraphes séparés. Il faut ajouter une ligne vide pour aller à la ligne puis deux espaces pour sauter une ligne.

5. **LES CITATIONS** / Il faut utiliserer le signe supérieur à (>) et l'utiliser autant de fois pour chaques lignes faisant partie de la citation.

> Ceci est un test.

6. **LES LISTES** / Il faut utiliser le signe plus, le tiret ou un astérisque. Ces trois options donnent le même rendu. 

Pour créer une liste numérotée, il faut inscrire un chiffre suivi d’un point.

Markdown permet aussi d’éditer des listes à cocher. Ces listes sont précédées d’une case à cocher pouvant être activées par clic. Ces cases peuvent d’ailleurs être cochées par défaut au moment de la création de la liste. Pour ce faire, il faut utiliser les crochets et le X. (il faut laisser un espace entre les deux crochets pour les cases à cocher vides)

- test 1
- test 2

[ ] A
[x] B
[ ] C

7. **LES TABLEAUX** / Les barres verticales (|) permettent d’éditer des tableaux avec Markdown. Chaque cellule du tableau est séparée d’une barre verticale. Pour créer des lignes de titre qui se distinguent du reste du tableau, il faut souligner les cellules concernées avec les tirets du 6.

|cellule 1|cellule 2|
|--------|--------|
| essai 1 | essai 1 |
| essai 2 | essai 2 |

Il n’est pas nécessaire d’aligner les barres verticales les unes en dessous des autres, les aligner rend le tableau plus lisible en Markdown.


8.  AJOUTER DES **EMOJIS** /

J'ai appris beaucoup de façon de créer dans "Ajoute des caractéristiques à ton document". Cela m'a permit par exemple d'ajouter des emoji.
J'ai pu aller dans "documentation.md" -> "Ecrire un document en Markdown" -> Lien vers FabZero Tutorial -> "Ajoute des caractéristiques à ton document" -> "Emoji cheat sheet"

Dans cette endroit on peut directement copier/coller l'encodage des emojis de notre choix en cliquant dessus.

par exemple :

:white_medium_small_square:
:cherry_blossom:


## INSTALLATION DE GIT SUR MacOs

J’ai fait quelques recherches sur GIT pour comprendre ce que j’allais installer sur mon ordinateur, ces fonctions et son utilité. La page Wikipédia permet d’avoir une vue d’ensemble de sa définition, son développeur Software Freedom Conservancy a créé sa première version le 7 avril 2005.

Je me suis demandé comment installer Git sur MacOs. Grâce au site de GIT, j’ai découvert que  plusieurs options s’offraient à moi : Homebrew, Xcode, Installateur binaire ou Construire à partir de la source.

J’ai cru comprendre que je devais l’installer à partir du terminal et non comme une application classique de MacOs.

Xcode 12 me paraît être le plus en cohérence avec l’utilisation d’un MacBook Pro car il est conçu par Apple Déveloper.  https://developer.apple.com/xcode/

J’ai essayé d’installer Xcode avec la commande « xcode-select —install » dans le terminal mais la procédure à échoué.

J’ai pu télécharger l’application Xcode 12.2 bêta 2 sur le site directement.

Une fois le logiciel installé, cette page s’est ouverte.

![](../images/GIT1.jpg)

Je suis ensuite retourné sur : Documents GitLab —> Guides de base de GitLab —> « Commencez à utiliser Git sur la ligne de commande ».

Je découvre beaucoup d’éléments et de termes de mon ordinateur dont je ne connaissais pas l’existence.

Je dois configurer Git et utiliser le Terminal comme Shell de commande. Quand j’inscris la commande « git —version », l’ordinateur m’indique « command no found ». J’ai alors tapé « git » pour voir ce qu’il allait me dire et Xcode a été reconnu. Le problème vient des outils de développement en ligne de commande qui ne sont pas installés. 

![](../images/GIT_2.jpg)

Je viens de me rendre compte que j’avais installé une version bêta. Je ne connaissais pas et je vois qu’il n’y a pas d’autre choix.

Vu que je suis bloqué, je suis allé regarder dans les réglages de sécurité et confidentialité. Dans « Outils de développement » ou apparait le logo de Xcode, la case autorisant les apps à exécuter localement un logiciel avec le logo du Terminal n’était pas coché. Je vais maintenant retenter de configurer Git sur le terminal.

![](../images/GIT_3.jpg)

Le terminal me renvoie toujours vers l’installation de logiciel, sans succès. Quand je lance l’installation des outils de ligne de commande, l’ordinateur me dit que l’opération est impossible.

![](../images/GIT_4.jpg)
![](../images/GIT_5.jpg)

J’ai tapé dans ma barre de recherche Google « Installer Xcode dans le terminal » et je suis tombé sur un article du site embarcadero.com « Installation des outils en ligne de commande Xcode sur un Mac ».

![](../images/GIT_6.jpg)

Dans le panneau General, n’ayant pas la touche Downloads j’ai cherché par moi-même dans les onglets, un composent qui pourrait m’être utile, comme une extension et je suis arrivée à l’onglet « Xcode extensions ».
Je télécharge alors Xcode à nouveau qui n’a pas l’air d’être la version Beta ou qui favorise l’utilisation de l’application.

![](../images/GIT_7.jpg)
![](../images/GIT_8.jpg)

Je n’ai pas besoin de réinstaller Xcode donc j’ai arrêté le téléchargement. Je devais simplement suivre la deuxième étape depuis le Web. En cliquant sur le liant je suis arrivée à cette page pour télécharger un « Outils de ligne de commande pour Xcode 12.2 beta 2.dmg »

![](../images/GIT_9.jpg)

J’ai enfin installé les outils de développement en ligne de commande.
Je suis ensuite retourné sur : Documents GitLab —> Guides de base de GitLab —> « Commencez à utiliser Git sur la ligne de commande » pour configurer Git depuis le Terminal.

![](../images/GIT_10.jpg)
![](../images/GIT_11.jpg)

Le terminal m’inscrit toujours « command no found ».

Je suis allé sur Documents GitLab —> Les sujets —> « Installer Git » et je me suis rendu compte que j’ai oublié de faire une étape et que je n’avais jamais accédé à ce document qui explique bien comment installer Git. Je dois installer Homebrew.

Homebrew est en installation en ayant suivi les instructions directement depuis le site :

![](../images/GIT_12.jpg)
![](../images/GIT_13.jpg)

J’ai pu « installer Git via Homebrew » ! Git fonctionne avec la commande « git—version », ma version est « git—version 2.28.0». J’ai pu configurer Git et ajouter mon nom d’utilisateur après m’être tromper. Il faut m’être les même informations d’identification que GitLab.

![](../images/GIT_14.jpg)
![](../images/GIT_15.jpg)

J’aimerais m’authentifier en utilisant SSH, il faut consulter les instructions de la documentation SSH pour le configurer avant clonage. Je dois ajouter une paire de clés SSH.

![](../images/GIT_16.jpg)

Grace aux Documents GitLab —> Guides de base de GitLab —> « Créez et ajoutez votre paire de clés SSH », j’ai pu suivre les instructions pour générer ma paire de clés SSH.

On me propose deux solutions « les clés ED25519 SSH ou les clés RSA SSH». Je choisis d’utiliser une clé ED25519 SSH. Après l’avoir configurer je dois inscrire une phrase secrète.

Si un jour je veux mettre à jour ma phrase secrète je dois utiliser « ssh-keygen -p -f /path/to/ssh_key ».

Je copie ma clef à GitLab avec « pbcopy < ~/.ssh/id_ed25519.pub » dans le Terminal et je l’ajoute à GitLab.

![](../images/GIT_17.jpg)

J’ai pu tester que tout été correctement configuré.

![](../images/GIT_18.jpg)

Après avoir écrit les étapes d'installion de Git sur ce document, je ne comprends pas pourquoi mes images ne s'affiche pas en fichier rendu mais en fichier source, la ligne de codage image, oui.. Il fallait juste ajouter ../ avant images, je me suis inspirer des autres images.






