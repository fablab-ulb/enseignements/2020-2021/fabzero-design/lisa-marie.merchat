# 3. Impression 3D

Cette semaine, j'ai pu utiliser pour la première fois une imprimante 3D. Son utilisation convoque plusieurs logiciels et connaissances qui nous étaient diffusés les Fabmanagers.

# LA FORMATION D'IMPRESSION 3D SUR PRUSA

### Qu'est-ce qu'une imprimante Original Prusa i3 MK3S?

L'imprimante dont nous disposons au FabLab est actuellement la meilleure des imprimantes Prusa. Une information particulière sur cette imprimante est que les imprimantes Prusa sont imprimées par des imprimantes Prusa. Ceci favorise une production automatique pour la marque qui réduit fortement son cout de fabrication.

![](../images/information_prusa.png)

© LMM

### Quels matériaux?

La machine fonctionne à partir de bobine de filament. Il existe 120 filaments en vente sur le site de Prusa: https://shop.prusa3d.com/en/16-filament, mais le FabLab dispose surtout de PLA et de filament de bois (difficile à utiliser et déconseillé).


### Comment procéder?

- Enregistrer le fichier Fusion360 au format STL
- Ouvrir Prusa Slicer et faire glisser le fichier sur le plateau
- Choisir la face de stabilisation pour l'impression (souvent la plus grande / éviter les renforts)
- On peut changer l'échelle de l'objet (à savoir : le plateau à une surface d'impression de 20x20x22cm)
- Procéder aux réglages (voir l'Aspect Technique)
- Exporter le fichier en G-code
- Mettre le fichier sur la carte SD de la machine correspondante en terminer un dossier avec son nom
- Passer de l'acétone sur le plateau avec un papier pour l'adhérence
- Vérifier que ce soit le fils que l'on souhaite ou le changer (voir l'Aspect Technique)
- Ouvrir le fichier et lancer l'impression


### L'Aspect Technique (Je prends l'essai n°2 comme exemple)

I / AVANT D'IMPRIMER L'OBJET - PRUSA SLICER

I / 1 - ONGLET "PLATEAU"


![](../images/explication_commande_prusa.png)

© LMM

I / 2 - ONGLET "REGLAGE D'IMPRESSION"

On peut demander aux FabManagers si tout correspond pour une impression optimale et elles nous guident sans soucis si l'objet nécéssitent des réglages particuliers.

- 1. COUCHE ET PERIMETRE / La hauteur de couche et la première couche est toujours 2 mm. Les parois verticales doivent assurer une certaine solidité et doivent être à 3 mm au minimum. Pour les réglages du dessous on peut les laisser par défaut.

![](../images/reglage_1_essai_2.png)

© LMM

- 2. REMPLISSAGE / Idéalement la densité de remplissage doit être comprise en 10 et 15 %. Le maximum est de 35 % mais cela gaspille de la matière. Les motifs de remplissages les plus efficaces sont Gyroïde et Nid d'Abeille. Dans mon cas je dois choisir un remplissage .... On peut laisser le reste par défaut, c'est-à-dire un remplissage du dessus et dessous rectiligne.

![](../images/reglage_2_essai_2.png)

© LMM

- 3. JUPE ET BORDURE / La jupe se programme par défaut pour assurer la meilleure stabilité. On peut rajouter une bordure pour avoir une plus grande emprise au sol sur le plateau.

![](../images/reglage_3_essai_2.png)

© LMM

- 4. SUPPORTS / Les supports ou renforts, permettent le maintien d'une partie de l'objet en porte à faux ou creux. Ainsi il ne tombe pas. On peut générer automatiquement les supports. Si la pièce est mal placée, il y aura plus de supports, c'est pour cela que c'est important de choisir la face de stabilisation pour l'impression.

![](../images/reglage_4_essai_2.png)

© LMM


II / PARAMETRES SUR LA MACHINE

![](../images/impression_essai_2.png)


# REALISATION DE L'IMPRESSION


## Premier essai

### Effectuer les différents réglages

J'ai pu me référer à ce qu'on fait mes camarades pour établir les réglages de Prusa. Notamment les renforts qui permettent de stabiliser la structure à imprimer. Dans mon cas je devais ajouter un renfort assez important pour que la demi-sphère ne s'effondre pas. Gwenn a pu m'apprendre comment faire les réglages des renforts pour qu'ils soient plus faciles à retirer, surtout dans une forme ronde.

Une fois les réglages finis, on peut exporter le fichier en GCode et le télécharger sur la carte SD d'une machine libre en s'affichant un dossier avec son nom.

Il faut mettre de l'acétone sur le plateau pour l'adhérence. Puis on peut lancer l'impression.
J'ai choisi de changer de couleur mon objet pour qu'il ne soit pas noir. Un camarade m'a montré la fonctionnalité pour changer le fils. C'est assez intuitif et la machine fait presque toute seule. Il ne faut pas oublier de couper le bout de fils avant de le mettre dans la buse. Une fois que la couleur qui coule est la bonne, on peut lancer l'impression.


### L'objet en cours de création

Lors de ce premier essai, j'ai pu voir l'objet en cours de réalisation pendant 1h30. Les renforts avaient une importance considérable dans la réalisation d'une coque comme celle-ci.

![](../images/renfort_impression.png)

© LMM

### L'objet finit

Les renforts on était assez simple a enlevé et je n'ai eu aucun problème pour la réalisation de cette impression.

![](../images/Essai_impression_3D_coque.jpg)

© LMM


## Deuxième essai

J'explique les différents réglages utilisés pour l'explication du fonctionnement de la machine.

Je ne suis pas satisfaite à 100% du résultat, ce qui est sur c'est que c'est chouette d'avoir pu imprimer une pièce dans la journée en ayant retourné la lampe dans Prusa Slicer.

Les renforts sont nombreux avec des formes coques... C'est le defaut d'une telle forme, sauf si elles sont séparées. Je pourrais créer les deux formes séparément pour qu'elles s'emboitent.

![](../images/essai_hack_prusa.png)





