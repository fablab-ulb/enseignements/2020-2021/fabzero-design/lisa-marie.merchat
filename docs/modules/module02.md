# 2. Fusion 360

La journée du 15 octobre 2020, j'ai pu réaliser une impression 3D d'un élément de l'objet que j'ai choisi au musée du design. La première étape consiste à modéliser l'objet sur Fusion360 après avoir eu une formation des éléments de base pour utiliser le logiciel.

Les semaines qui ont suivi j'ai pu me perfectionner dans l'utilisation de Fusion 360, soit juste pour modéliser et avoir une vision claire du projet et soit pour créer une autre impression.

# FORMATION FUSION 360

### Fusion360 ?

Tout d'abord il faut télécharger le logiciel. Celui-ci n'est malheureusement pas gratuit, sauf si vous êtes étudiant. Vous pouvez le télécharger à partir d'Autodesk ici : https://www.autodesk.com/products/fusion-360/overview

C'est un logiciel conçu pour le développement du produit de l'ingénierie, l'électronique et la fabrication. Sur le site il le décrive comme un outil pour l'impression 3D métal, les pièces usinées CNC et les circuits imprimés. Mais dans sa globalité, le logiciel permet tout type de 3D technique.

![](../images/fusion_logo.png)

### Apprendre à utiliser les outils et premiers essais

Lors de la formation nous avons modélisé 2 pièces, une forme et une tour d'échec.

![](../images/premier_essai_fusion.png)

J'ai appris des raccourcis qui sont utiles pour aller plus vite dans son travail :

- d -> côter
- l -> créer une ligne
- t -> supprimer un bout de ligne
- c -> créer un cercle
- r -> créer un rectangle
- e -> extrusion
- f -> congé

Les éléments qui peuvent nous aider appris pendant le cours :

- mettre une ligne de construction qui n'intervient pas dans l'objet
- faire un perçage avec un réseau circulaire


### Parcourir les différents onglets

![](../images/onglets_fusion.png)

![](../images/essai_mise_en_page.png)


Zoom sur l'onglet animation qui permet de faire des films ou des éclatés de l'objet. Cela permet de montrer des détails si la pièce est complexe.

![](../images/zoom_animation.png)

# CONNAITRE L'OBJET CHOISI POUR LA MODELISATION : LA LAMPE DE TABLE TELEGONO

Il est nécessaire de connaitre l'objet que l'on a dû choisir au musée du Design pour le reproduire sur Fusion.

Les informations que j'ai par rapport à la lampe de table sont très différentes. On peut facilement acheter la lampe d'origine sur des sites de vente de produits design.

J'ai trouvé les tailles :

- Longueur : 25
- Largeur : 20
- Hauteur : 38

J'ai tenté d'établir un profil à taille réduite pour exécuter une révolution et obtenir la forme. Je l'ai découpé en 3 parties mais avec du recul j'aurais pu le faire en une seule fois... Erreur de débutante...

![](../images/dessin_de_la_lampe_decoupe_3D.png)


# REALISATION DE L'OBJET SUR FUSION 360

## Premier essai

J'ai redessiné une partie de l'objet. Vu que je n'ai utilisé qu'une seule fois Fusion360, j'ai eu du mal à exécuter une simple forme. J'ai choisi d'isoler l'élément qui me semblait principal. La base de la lampe est une forme réalisée en une fois, ca me semblait intéressant d'utiliser l'impression.

Pour faciliter la modélisation, j'ai décomposé l'objet en 3 parties que j'ai ensuite rassemblées et liées.
J'ai fait plusieurs essais avant de trouver une solution très simple. Je dois dessiner le profil de la base puis faire une révolution de 180 degrés. J'ai passé l'après-midi avant de comprendre ça.. J'ai perdu beaucoup de temps mais est compris un peu plus l'ensemble du logiciel. Les fonctionnalités sont finalement assez intuitives en comparaison avec Archicad que j'utilise depuis 5 ans.

![](../images/sample-pic-2.jpeg)

Vous pouvez voir dans le module 3 l'impression 3D de la coque de lampe. Il m'était très difficile lors de ce premier essai d'utiliser Fusion 360. J'ai alors compris qu'il suffisait de partir de formes simples.


## HACKER L'OBJET

Pour ce deuxième essai, je souhaite réaliser mon objet en entier avec un mécanisme de rotation comme j'ai pu voir dans les travaux d'autres camarades. Les machines 3D me paraissaient beaucoup plus restreintes. Grâce à un site d'objet 3D, j'ai trouvé la lampe telegono modélisée en détail.

![](../images/lampe_hack.png)


# PROTOTYPE DE GUIRELANDE

Pour le projet en découpe laser, j'ai pu réaliser un prototype de tête de guirlande. Cela m'a permis de voir si les encoches étaient bien réalisés et ce que rendait l'objet de petite taille, qui était assez dur de réaliser en papier. Les outils de textures m'ont réellement aidé et permettent de s'identifier au projet final. Le projet est visualisable dans la partie 4 où j'explique comment réaliser une découpe laser.


![](../images/guirelande_fusion.jpg)

©LMM / Fusion 360




