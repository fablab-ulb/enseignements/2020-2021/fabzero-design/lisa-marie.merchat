### LE PROJET FINAL - La lampe Sens '

![](images/logo_3.png)


# NOTE D'INTRODUCTION DE L'OBJET FINAL

Cette page reprend l'ensemble des étapes qui constituent mon projet final. Ce projet correspond à une inspiration issue de toute la démarche de projet réalisé depuis le Brussels Design Museum jusqu'à l'objet final. L'inspiration est issu d'analyses où je me suis intéressé à **la modularité, l'intériorité, le visible et l'invisible, et la géométrie des formes**.

L’objet final est la lampe Sens’, inspiré de plusieurs références comme la lampe Telegono et les oeuvres de James Turell. Elle est issue de l’analyse de formes géométriques, de la trajectoire de la lumière et d’un questionnement chromatique. D’une forme angulaire à la ligne courbe, le design s’est épuré au fil du processus de conception.

Elle se compose d’un élément imprimé en PLA qui s’illumine grâce à un mécanisme intérieur. Ainsi la coque colorée rayonne et offre une ambiance lumineuse en devenant translucide à l’allumage. Le principe est que la lampe soit transportable et rechargeable. Elle est modulable car elle peut être utilisée seule ou en ensemble et intègre un système d’accrochage par aimant.

L’usager participe à la conception par une personnalisation en l’imprimant de la couleur et de la taille de son choix. Cette lampe est sensuelle autant par sa forme que par les sens qu’elle convoque : la vue, le toucher et la dimension ludique, c’est un appel au sens de la créativité et la sensibilité.

![](images/intro_git.png)

# RECHERCHES

## L'INSPIRATION

D'après Wiktionnary : (Par extension) Acte de **stimulation de l’intellect**, des **émotions** et de la **créativité** à partir d’une influence. / (Art) Idée créatrice, élan créateur d'origine **mystérieuse**.

D'après lintern@ute : Idée qui pousse à la création. Synonymes : **invention**, impulsion, **imagination**, révélation, trouvaille

D'après le Larousse : **Enthousiasme**, souffle créateur qui anime l'écrivain, l'artiste, le chercheur : Chercher l'inspiration. /**Influence** exercée sur un auteur, sur une œuvre : Une décoration d'inspiration crétoise.

Selon moi : l'inspiration c'est lorsque qu'un élan de motivation, donne l'envie de créer. Par un objet de référence, l'inspiration peut naitre.


## MOODBORDS ET PREMIERES RECHERCHES

Vu que je n'avais pas d'inspiration créative. C'est en **observant et analysant** l'objet que j'ai choisi au musée qu'une première inspiration m'est apparue. C'est **par le dessin que je stimule ma créativité**. Ces premiers moodboards et ces premières idées spontanées, m'ont dirigée vers un nouvel apsect du concept que j'ai envie de mener. J'aimerais travailler sur **la moduralité d'usage et la lumière**. Je juge trop complexe la conception que j'ai dessinée. Je dois **affiner le concept** et je réflechis sur les oeuvres de l'artiste James Turrell qui produit des oeuvres minimalistes mais très fortes dans leur dimension spatiale et lumineuse.

![](images/analyse_lampe.png)

![](images/premieres_inspirations.png)

![](images/moodboard2.png)

![](images/inspiration2.png)

![](images/dessins_minimalistes.png)


## REF JAMES TURRELL

La **lumière** détourée, comme emprisonnée ou faisant partie du **support**. Cette référence m'a beaucoup intéressé dans sa dimension spatiale et son concept d'intériorité. Cela m'a permis de réfléchir par le dessin à un concept et à l'évolution de mon idée. Je me demande comment reproduire une lumière intériorisée. J'aime le fait qu'on puisse **inclure le spectateur dans une ambiance**. Il parait que l'on est absorber par ses réalisations. 

![](images/james_turrell.png)


## LE CONCEPT ET PROTOTYPES - 1

J'aimerais que ma réalisation finale soit **minimaliste**. Les expérimentations que je vais pouvoir réaliser au FabLab me permettront de les simplifier au fil des semaines. Les formes **géométriques** sont la base de la création. Très minimalistes, elles **permettent une grande stabilité ou instabilité**. J'aime l'aspect **dépouillé** qu'elles procurent. Combinées, elles offrent une **diversité d'usages**.

Pour que mes lampes soient les plus simples possibles, je vais tenter de la réaliser en trois pièces. La majeure partie **pleine et creusée en impression 3D**, une **feuille plastique translucide** découpé au laser et la lampe led viendrait se nicher à l'intérieur de la lampe. Si je peux réduire les éléments par la suite, ça me permettra de réduire **le temps de fabrication**. Pour cela, je vais devoir réaliser des prototypes sur Fusion 360. Le concept doit être **reproductible facilement** et doit avoir le moins d'étape de fabrication possible. Je réalise des prototypes à l'échelle 1/2 à l'aide de l'impression 3D. Ainsi je peux me rendre compte de l'aspect dimensionnel et physique. Par l'évolution du premier modèle, j'arrive à un modèle modulaire dans sa dimension spatiale et lumineuse. Ces prototypes imprimés sur le même plateau font 8h d'impression.

Le test 3, le plus **évidé**, permettra une meilleure diffusion de la lumière et offre plusieurs scénarios d'usages. Sans le faire exprès, deux prototypes se sont collés lors des prises de vues et j'ai été inspiré une nouvelle fois : je peux **multiplier les scénarios d'usages** si la lampe est jumelable en copiant le modèle. Je me demande alors si la lampe doit avoir un système d'accroche entre deux modèle ou si la juxtaposition suffit.

![](images/evolution_projet.png)

![](images/piste_selectionnee.png)

![](images/modularite_avec_deux_lampes.png)


## PROTOTYPE - 2

La réalisation d'une lampe à l'échelle 1:1 me permet de mieux appréhender les dimensionnements et les éléments que je vais pouvoir ajouter pour accorder les éléments entre eux. Sachant que la lampe ne va peut être ne pas garder ce dimensionnement. L'imprimer à **grande échelle** m'a permis d'avoir une idée de la **prise en main de l'objet**. Ces reflexions directement sur la matière ont été très utiles pour intégrer la lumière, les systèmes de liens et les écrans. Je réfléchis sur la **technique** que je vais pouvoir intégrer à ma lampe pour qu'elle s'éclaire. Intégrer une ampoule ? une bande LED ? une guirlande ? une sortie de câble ou une option de recharge ? évider pour intégrer des aimants ? Emboitement ? Encoches et fixations ? sans écran ?

![](images/essai_echelle_reelle.png)


## RECHERCHES TECHNIQUES

Les différentes recherches sur l'électricité m'ont conduit vers plusieurs pistes : utiliser plusieurs **bandes LED** ou une seule. Un **circuit à branchement ou un circuit rechargeable**. Mais je ne trouve pas de détail technique sur la lampe rechargeable, seulement des références comme la Lampe FAT qui est une des pièces de la fameuse série de luminaires conçus par Tom DIXON. Je pense que je vais devoir **démonter une lampe rechargeable** pour comprendre son fonctionnement.

![](images/recherches_elec.png)

Pour mener à bien mon projet et intégrer un système rechargable, j'ai acheté une **lampe de vélo** que je vais décomposer. La lampe rechargeable **la plus petite et puissante du Brico** coûte 11,99 €. A partir de la décomposition de cette lampe, je vais pouvoir analyser son circuit. Malheureusement lors de la décomposition j'ai cassé une pièce du circuit imprimé mais il correspond à mes attentes, c'est une première piste. Je vais pouvoir en acheter plusieurs car le **système compact** de la lampe convient à mon projet. La lampe se compose d'une batterie rechargeable, d'un bouton poussoir, d'une barre de LED et en bonus, différentes intensités d'éclairages, fixe ou clignotant.

![](images/lampe_decomposee.png)

![](images/Intensite-lampe.mp4)

J'aimerais tenter d'**intégrer ce circuit à ma lampe** mais tout d'abord, je me demande comment allonger la longueur du fil qui raccroche le circuit imprimé à la LED. Je suis retourné au Brico pour acheter: un fer à souder, de la soudure étain et du fil d'installation basique pour rallonger les éléments. J'ai notament repris une lampe à vélo au cas ou je n'arrive pas à réparer la première. J'explique dans cette nouvelle partie comment souder une pièce, quels sont les règles et comment procéder.

**Un fer à souder** : il n'est pas nécessaire d'acheter un fer haut de gamme, le premier prix coûte 14,99€ au Brico.
A savoir : l'embout du fer à souder s'appelle une panne. Pour de la soudure électronique, il faut choisir une panne "conique" qui est un embout de précision.

**La soudure étain** : la soudure étain est du fil en étain qui, une fois fondu, permet de raccorder des éléments entre eux. Pour une bobine "Standard", cela coûte 7,99€ au Brico.
A savoir : NE PAS choisir de la soudure à l'acide, cela endommagerait vos circuits et vos composants.

**Le fil d'installation** : il permet de connecter les éléments entre eux, le fil de base est un fil de 1,5 mm utilisable pour les ampoules et connexions éléctroniques. Pour une longueur de 5 m, la bobine coûte 4,19€ au Brico.

**La tresse de dessoudage** : c'est un fil tressé en cuivre ou en laiton.

![](images/ensemble_soudure.png)


SOUDER LES ELEMENTS ENSEMBLES

Se préparer **avant de souder** : avant de souder, il faut préparer son plan de travail. Pour cela il faut : de la place, un support de fer à souder (fourni avec le fer), une éponge humide, des lunettes transparentes. Le support permet de ne pas faire bruler la table ou bien vous-même, l'éponge permet de nettoyer la panne et TRES IMPORTANT, les lunettes transparentes protègent vos yeux d'éventuelles éclabousures détain fondu.

**Désouder** : enlever de la soudure pour changer des composants, il faut utiliser une tresse à dessouder. Je n'en ai malheureusement pas trouvé au Brico.. Placer un morceau de tresse à dessouder sur le dessus de la soudure à enlever. Une fois le fer à souder chaud, appliquez la panne sur la tresse. La soudure va être chauffée et absorbée par capilarité. Ensuite, enlever la tresse pour vérifier si la soudure a bien été enlever, si nécessaire on peut renouveler l'opération.
A savoir : la tresse devient très chaude, la tenir avec une pince et couper ensuite le bout avec une pince coupante. Si vous n'en avez pas comme moi, faites tout simplement chauffer la panne sur la soudure existante, elle fondra et s'écoulera.

**Etamage de la panne** : cela peret de protéger la pointe et réduire son usure. Vérifiez que la panne est fermement accrocher au fer. Branchez le fer à souder et laissez le chauffer, attention le fer chauffe à plus de 400 degrés. Essuyer la panne du fer sur une éponge humide pour la nettoyer puis attendre que la pointe rechauffe avant de faire l'étape 4. Tenir le fer à une main, toucher la pointe du fer à souder avec la soudure et vérifier que la soudure se repartit uniformément sur la pointe. Etamer la panne AVANT et APRES chaque soudure pour avoir un travail précis et une plus longue durée de vie de la panne.

**Souder** : Les connexions s'ils y en a, comme une LED à insérer dans la carte imprimer, les établirs et plier les fils à 45 degrés. Dans mon cas, je vais remplacer les fils. Chauffer la jonction : Tout d'abord il faut étamer l'extrémité de la panne (pour augmenter le contact thermique). Ensuite placer la panne  sur la jonction pendant 3 à 4 secondes. Appliquer la soudure à la jonction : comme mentionné à l'étape 2, il faut maintenir le fer à souder sur la jonction, IMPORTANT, le pas mettre la soudure sur la panne, mais sur la jonction qui doit être assez chaude pour faire fondre la panne, sinon cela établira une mauvaise connexion. Laisser la soude refroidir, IMPORTANT, ne pas souffler sur la jonction, cela provoquerait une mauvaise connexion

J'ai appris seule à me servir du fer à souder pendant plusieurs heures avant de tenter ces deux essais de soudures sur les circuits. Si vous devait souder aussi, entrainez-vous à souder deux fils ensembles et que la soudure soit fine et brillante. Ce n'était pas forcement le cas pour moi au moment des photos car c'était la première fois, mais avec un peu d'entrainement, j'ai reussi à faire des soudures plus fines.

![](images/soudure_essai_1.png)

![](images/soudure_essai_2.png)


## PROTOTYPE - 3

Observé sur les oeuvres de James Turrell et la Lampe Telegono : on ne sait pas d'où vient la lumière; C'est un système qui permet de procéder sans écran. Ainsi, par modification de **trajectoire de la lumière**, on peut éclairer une surface et obtenir une **lumière indirecte**. Je vais essayer de créer cet effet sur ma lampe dans un nouveau modèle imprimé en 3D. Pour intégrer le système de lampe rechargeable, je me base sur le système que j'ai soudé. C'est à l'aide des dimensions que j'ai pu réaliser un modèle Fusion 360 en vue d'une impression 3D. Je me suis inspiré de mon réveil IKEA pour rendre le système invisible. Le principe de fermeture du système électrique est intéressant et à conserver pour la suite. L'encoche pour le **bouton d'allumage** offre un rendu de finition à la lampe. La **sortie pour branchement** est également aux bonnes dimensions. Il y a tout de même des éléments qu ne fonctionnent pas et qui sont à faire évoluer comme la **trajectoire de la lumière**, **l'emplacment du système électrique** et l'échelle de la lampe. C'est un modèle massif et imposant et la disposition de la sorties de lumière offre plus d'ombre que de diffusion. Ainsi, pour obtenir un éclairement sans zone d'ombrage il faut repenser la trajectoire de la lumière en premier, ce qui induira l'emplacement du système technique.

![](images/integration_lumiere_1.png)

![](images/essai_integration_lampe_1.png)

![](images/film_lumiere_integre.mp4)


## MAQUETTE TRAJECTOIRE

Ces maquettes en papier plié on était très bénéfique pour comprendre le système de fonctionnement de la lumière et de la petite lampe LED. Mais le papier ne renvoie pas la même lumière que du PLA qui est assez translucide en fonction des couches.

![](images/trajectoire_lumiere.png)

![](images/trajectoire_lumiere_2.png)

## REF STEVEN HOLL, MUSEE DE CASSINO

Le musée fut construit dans le cadre d'expositions historiques et contemporaines. Stenven Holl souhaitait que le bâtiment n'ait **pas de fenêtre**. Après un travail sur la **trajectoire de la lumière**, il decide de créer un contraste fort entre l'intérieur et l'extérieur par des fentes. Ainsi **la lumière se propage** en fonction du temps qui passe et des saisons.

![](images/casino.png)

## REF EGLISE SAINT-PIERRE DE FIRMINY

SCENOGRAPHIE A L'OCCASION DE LA "NUIT DECALEE", 2014

Une scénographie lumineuse pour un week-end qui est une **allusion poétique** au passage du monde terrestre vers le monde céleste. Ils ont choisi une **palette de couleurs** dégradées du bleu au rose au violet. Il exprime une allégorie du ciel et un brouillard diffus avec une constellation d'étoiles projetées au sommet de la nef. L'installation offre à l'usager dans une dimension sensorielle à la lumière.

![](images/eglise.png)


## PROTOTYPE - 4

Le modèle sur lequel je travaille est **épais avec de fortes arrêtes**. Je me questionne pour **affiner la forme**. Lors de la séance du 3 décembre nous avons réfléchi a une **forme ouverte**, comme un pliage; Actuellement je travaille sur un **modèle noir**. Utiliser un fils PLA de couleur pourrait **transformer la lampe**. Ces **prototypes de couleur** m'ont permis de réaliser qu'une lumière sous forme de socle diffuse la lumière et crée une ambiance. A travers ces trois impressions, je peux opérer une petite évolution du modèle mais j'ai besoin d'avancer pour me sortir de la **forme trop rigide**. A cette échelle on ne voit pas bien les arretes congées du test 3. Mais ce que m'apprend mon modèle c'est que **je n'ai pas besoin de toute les faces**.

![](images/conception_de_la_forme.png)

![](images/proto_illumines.png)


## MAQUETTES DU PRINCIPE D'ECLAIREMENT

J'ai une **remise en question de la source** lumineuse et je ne sais pas si je dois continuer d'utiliser la lampe de vélo LED ou si je dois passer à une guirlande LED beaucoup plus puissance pour faire un socle. Le fait d'avoir **illuminé** le prototype 4 à l'aide une lampe de téléphone à fait briller la lampe et m'a donné envie de reproduire cet **effet**, qui avec seulement une impression 3D crée **une lampe d'ambiance**.

Le premier test est l'idée d'éclairer la lampe par un socle. Cette maquette n'a pas fonctionné étant donné que ce n'est pas **une lumière par contact direct**. Pour la seconde je me suis dit que peut être la lampe pourrait s'illuminer si elle faisait partie intégrante du socle et qu'elle dépasse en dessous et au dessous, mais la lampe ne s'éclairait que dans la partie non visible. Cela n'était pas le rendu attendu. La dernière maquette est issu de la réflexion sur **la lumière par contact**. Il reprend les idées précédentes avec la nouvelle forme. L'enjeux est maintenant d'**intégrer la lumière dans un nouveau prototype**. Vu que la lampe emet une **lumière tamisées**, je pense que des **couleurs chaudes** seraient idéal. Je décline la proposition en plusieurs couleurs et je vais effectuer un prototype 5 qui peut **jouer de ces couleurs**.

![](images/3_tests.png)

![](images/dessin_couleurs.png)

## PROTOTYPE - 5

Ce cinquième prototype est beaucoup **plus fin** que les précedent et **intégre un système lumineux**. Comme on peut voir sur ces shémas techniques, la coque imprimé en 3D est dotée d'éléments servant la fixation ou l'utilisation du système intérieur. La combinaison de plusieurs couleurs de lampes permettrait de créer des **ambiances différentes**. Grâce à ces nouveaux prototypes, je constate que **les couleurs rouge et orange sont très similaires** et qu'elles foncent ou s'éclaircissent en fonction de leurs positions. J'ai pu réduire considérablement le temps d'impression, nous sommes à 5h d'impression pour une lampe avec son système de fermeture. Le **système de fermeture est trop petit et ne s'est pas correctement imprimé**, je vais devoir repenser cet aspect fonctionnel pour le prototype 6, sauf si **je decide que la lampe ne souvre pas**. Je me demande si c'est nécessaire que le boitier s'ouvre vu qu'il n'y a rien à modifier à l'intérieur. La lampe est rechargeable par câble donc il n'y a aucun raison d'ouvrir la lampe. J'ai **collé la face de fermeture**. Le boitier intérieur mériterait d'être un peu plus large pour une meilleure fermeture. Il reste aussi à **régler la hauteur de la lampe**, actuellement je travaille sur un petit prototype avec un bon dimensionnement du boitier, mais je pense que je vais devoir faire des **test de dimension pour la suite**.

![](images/prototype_5.png)

![](images/prejury_1.png)

![](images/prejury_2.png)

![](images/plusieurs_dispositions_sens.png)

## MAQUETTES D'ACCROCHAGE MURALE PAR CLOUS

Ces maquettes permettent de **dimensionner** correctement les encoches pour une accroche murale de la lampe. Le percement se situerait dans une partie plate de la lampe mais qui sera inclinée. Tout d'abord, il faut choisir quel type de fixation avant de réfléchir à la largeur du percement. J'ai essayé les différentes tailles de clous, en vue d'une adaptation large et standard. Je me suis rendu compte avec cet essai de **stabilité** que le système de fixation doit avoir un retour. La lampe doit se fixer dans tous les **supports**, la fixation qui convient le mieux est le **gond** car les vis ont besoin d'une cheville et compliquent l'installation. Pour la première maquette j'ai utilisé un gond à visser puis j'ai pu trouver des gonds à clouer, beaucoup plus petit que les premiers. Pour que la fixation soit toujours optimale il faut que la fixation optionnelle fasse partie de l'achat. Ainsi, la lampe, accompagné d'un cordon micro-usb doit aussi contenir **2 gonds à clouer** avec une **fiche technique** d'aide au fixage.

![](images/maquette_clou.png)

## MAQUETTES D'ACCROCHE MURALE PAR AIMANT

Ces maquettes sont le retour sur l'idée du prototype 2. L'assemblage de différentes lampes entre elles se fait actuellement sans connexions. Je ne pense pas que cela soit finalement nécessaire car la juxtaposition des lampes permet un grand nombre de scénarios d'usage. Mais je pense que le système aimanté est un bon moyen de fixation, rapide et sur des endroits très vaste.

## PROTOTYPE - 6

Lors de la réalisation du prototype 5, je me suis rendu compte d'un **mauvais dimensionnement du boitier** et de la sortie câble micro-usb, ainsi, on ne pouvait pas fermer la lampe. Je me questionne notamment sur la tache lumineuse que le système produit lors de l'éclairement par contact. Je pense qu'il faudrait **épaissir la paroi** de contact pour réduire sa visibilité. Les changements à opérer pour le prototype 6 sont une amélioration de ces dimensions et l'intégrer une **nouvelle fonction d'accrochage murale**. Lors de l'impression du nouveau fichier, j'ai eu systèmatiquement des **problèmes d'impression**. J'ai rééssayé 4 fois d'imprimer mais se fut sans succès. Heureusement, j'ai assez d'informations sur les nouveaux dimensionnements pour les corriger. Les changements effectués sur la lampe nécessitent des **ajustements pour une meilleure intégration** à la coque. La **fermeture par cache** doit être **réduite en largeur**. Une parties du percement du **bouton d'allumage** doit être **affiné** et pour une raison d'estétique de même sur le **percement micro-usb**. Le dernier changement est le percement de l'accroche murale qui n'a pas besoin d'encoche.
Lors du rachat de nouvelle lampe, j'ai constaté qu'il existe des **feux arrières de vélos rouge** qui sont idéaux pour la lampe. Ils disposent des même carctéristques techniques mais réduit l'**effet de halo** sur la surface de contact.

Le prototype avec le feu arrière rouge est un résultat final de dimensionnement. Pour celui ci, j'ai pensé à une **transformation colorimétrique**. Une **inversion** qui crée un **effet de surprise** : la **lampe blanche** devient **rouge à l'allumage** par le contact de la **lumière rouge**. Quelques percements sont retravaillés pour une meilleure intégration des plugs à la coque. Le bouton d'allumage nécessite une épaisseur plus fine que l'épaisseur de la lampe pour l'emboitement et l'entrée du cable micro-usb pour le rechargement nécessite plus d'espace. **Deux renfoncements** permettent cette **intégration**. La modification à effectuer pour le prototype final est **le cache de fermeture qui doit être évider** sur quelques centimètres au centre pour être plus flexible pour une fermeture définitive plus efficace et plus rapide à la fabrication et y intégrer un aimant pour la fixation murale. Il faudra également enlever les trous qui permettent la fixation. **Augmenter le volume** permettra d'avoir une lampe plus imposante, pour ainsi créer une **ambiance plus large**, dans un agrandissement du petit modèle, une version grand modèle.

![](images/prototype_6_a_7_dessin.png)

![](images/prototype6.png)

![](images/prototype7.png)

## PERSONNALISATION ET MULTIPLICATION

Pour créer une ambiance plus immersive, on multiplie le petit modèle. Sur une table, le modèle remplacerait la bougies. Le principe est de pouvoir imprimer la lampe de la couleur de son choix associé au kit technique d'allumage et de rechargeament. Ainsi, on obtient des ambiances différentes selon les colorimétries de lampes.

![](images/multiplication.png)
![](images/multiplication_2.png)

## LE PETIT MODELE

Ce prototype est le **modèle final** qui intègre toutes les caractéristiques de dimmensionnement.

![](images/petit_modele_final.png)
![](images/petit__modele_explication.png)

## LE GRAND MODELE

L'idée est maintenant de **changer d'échelle** pour obtenir une série de lampe. Pour cela, il est nécessaire d'inclure un **deuxième système d'allumage** pour avoir un **éclairement diffus**. La taille du boitier se multiplie par deux en hauteur mais ne change pas de dimensionnement en sa base, a contrario, l'arc double de volume. 

![](images/grand_modele.png)
![](images/grand_modele_explication.png)

## LA FABRICATION

Caractéristique du système d'allumage : Lampe de vélo (Brico)

Caractéristique du filament d'impression : PLA Filament 1,75mm


**COÛT**

Prix du système d'allumage : 11,99€

Coût d'impression : entre 0,7€ et 2€


**TEMPS**

Temps d'impression du petit modèle de lampe : 4h

Temps d'impression du grand modèle de lampe : 11h20

Temps de montage de la lampe : 3 mins

## LES FICHIERS POUR REPRODUCTION

Pour une optimisation de rendu, je vous conseil d'utiliser les fichier GCode qui sont déjà paramétré dans PrusaSlicer. Si vous devez imprimer sur une autre imprimante 3D, vous devez vous même paramétrer les fichiers. Voici quelques réglages pour obtenir les même résultats.

**Les fichiers STL**

[Fichier Grand Modele](images/grand_modele.stl) / [Fichier Petit Modele](images/petit_modele.stl)


**Les fichiers GCode**

[Fichier Parametré Grand Modele](images/grand_modele_0.2mm_PLA_MK3S_11h16m.gcode) / [Fichier Parametré Petit Modele](images/petit_modele_0.2mm_PLA_MK3S_4h.gcode)

![](images/prusa_final_reglage.png)
![](images/prusa_final_reglage2.png)

## COMPOSITION DE LA LAMPE DE VELO

Ces lampes de vélos après décompositions sont les **modèles idéaux**. La coque de la lampe fut **concut à partir de ce système**. Il est nécessaire de connaitre sa composition dans une démarche de commercialisation, nous devrions pouvoir recréer le système. Ce système est composé d'un **circuit imprimé** où plusieurs plugs permettent la lumière et le chargement. Il y a une **batterie de 3.7 V**, une **sortie micro-usb** et une **micro lampe led**. Ci-dessous, voici un shéma de composition du circuit imprimé.

## CREATION D'UN LOGO ET NOM DE L'OBJET

Le premier nom de la lampe m'est venu en observant le profil de la lampe, elle m'a fait penser à la forme d'un coquillage qui est aussi composé d'une coque. La coque est alors une coquille intégrant de la lumière, comme un coquillage intégrerait une perle.

Le nom n'est pas vraiment approprié car le coquillage n'est pas révélateur de la forme mais "ressemble à". Donc il me semble plus judicieux de changer le nom de la lampe. Pour cela me questionne sur ce que le **forme de la lampe** et ses caractéristiques renvoient. La forme est **courbe** et la lumière est une lumière d'**ambiance**. Cette lampe est **sensuelle**, autant par sa forme que par les **sens qu'elle convoque** : la vue et le toucher. Elle apporte une **dimension ludique** à l'usager qui peut composer un ensemble de lampes, un appel à son **sens de la créativité** et sa **sensibilité**.

Quelques définitions des **Sens'**:

**Sensuel** : (D'après l'internaute) Agréable pour les sens. Qui flatte les sens, qui les éveille.

**Sensitive** : (D'après l'internaute) qui transmet des sensations / qui est sensible

**Sensibilité** : (D'après l'internaute) Caractère d'une personne qui est réceptive psychologiquement, moralement, esthétiquement. / Caractère d'une personne qui ressent des sensations physiques.

La lampe Sens est écrite avec la police Andale Mano  avec un dégradé de couleurs de l'orange au rose.

![](images/shell_concept.png)

![](images/evolution_logo.png)

## FICHE DE PRESENTATION

![](images/Merchat_Fiche_de_presentation.jpg)

